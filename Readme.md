helm install mychart --dry-run --debug demo  
helm install mychart --debug demo
helm uninstall mychart

export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=demo,app.kubernetes.io/instance=mychart" -o jsonpath="{.items[0].metadata.name}")
export CONTAINER_PORT=$(kubectl get pod --namespace default $POD_NAME -o jsonpath="{.spec.containers[0].ports[0].containerPort}")
echo "Visit http://127.0.0.1:8080 to use your application"
kubectl --namespace default port-forward $POD_NAME 8080:$CONTAINER_PORT

helm install phoenix-chart demo/ --values demo/values.yaml
helm uninstall phoenix-chart


export NODE_PORT=$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].nodePort}" services phoenix-chart)
export NODE_IP=$(kubectl get nodes --namespace default -o jsonpath="{.items[0].status.addresses[0].address}")
echo http://$NODE_IP:$NODE_PORT

kubectl --namespace default port-forward  8080:$CONTAINER_PORT

kubectl get pod phoenix-chart-mysql-0 --template='{{(index (index .spec.containers 0).ports 0).containerPort}}{{"\n"}}'

kubectl port-forward phoenix-chart-mysql-0 28000:3306

mysql -u admin -p test -h 127.0.0.1 -P 28000